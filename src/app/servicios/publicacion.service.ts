import { Injectable } from '@angular/core';

@Injectable()
export class PublicacionService {
  constructor() {
    console.log('Servicio listo para utilizarse');
  }

  getAll(): any {
    let localPosts: Publicacion[] = [];
    localPosts = JSON.parse('[' + localStorage.getItem('publicaciones') + ']');
    return localPosts;
  }

  getId(): number {
    let id = 0;
    for (const elemento of JSON.parse('[' + localStorage.getItem('publicaciones') + ']')) {
      if (elemento.id >= id) {
        id = elemento.id + 1;
      }
    }
    return id;
  }

  devolverPubli(publi: number): any {
    for (const elemento of JSON.parse('[' + localStorage.getItem('publicaciones') + ']')){
      if (elemento.id.toString() === publi.toString()) {
        return elemento;
      }
    }
  }
}

export interface Publicacion {
  id: number;
  titulo: string;
  cont: string;
  img: string;
  fecha: string;
  likes: number;
  dislikes: number;
}
