import { Injectable } from '@angular/core';

@Injectable()
export class LoginService {
  usuario: Usuario = {
    user: '',
    pass: '',
    rol: '',
  };

  constructor() {
    console.log('Servicio listo para utilizarse');
  }

  private users: Usuario[] = [
    {
      user: 'admin',
      pass: '123',
      rol: 'admin',

    },
    {
      user: 'user',
      pass: 'pass',
      rol: 'user',

    }
  ];

  public encontUsuario(): any {
    let usuario: Usuario;
    usuario = null;
    for (const elemento of this.users){
      if (elemento.rol === '') {
        usuario = elemento;
        return usuario;
      }
    }
    if (usuario === null) {
      return false;
    }
  }

  public compUsuario(user: string, pass: string): boolean {
    this.usuario.user = user;
    this.usuario.pass = pass;
    for (const elemento of this.users){
      if ((elemento.user === this.usuario.user) && (elemento.pass === this.usuario.pass)) {
        this.usuario = elemento;
        const jsonstring = JSON.stringify(this.usuario);
        localStorage.setItem('usuario', jsonstring);
        return true;
      }
    }
  }

}

export interface Usuario {
  user: string;
  pass: string;
  rol: string;

}
