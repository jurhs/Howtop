import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LoginService, Usuario } from '../servicios/login.service';
import { PublicacionService, Publicacion } from '../servicios/publicacion.service';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {
  sesion = 'Iniciar Sesion';
  usuario: Usuario = {
    user: '',
    pass: '',
    rol: '',
  };
  publicaciones: Publicacion[] = [];
  eliminar = false;
  id: string;

  // tslint:disable-next-line:max-line-length
  constructor(private router: Router, private activatedRoute: ActivatedRoute, private loginService: LoginService, private publicacionService: PublicacionService) { }

  ngOnInit() {
    if (this.usuario.user === '') {
      this.usuario = JSON.parse(localStorage.getItem('usuario'));
    }
    this.publicaciones = this.publicacionService.getAll();
  }

  verPublicacion(actual: string) {
    this.router.navigate(['/publicacion', actual]);
  }

  eliminarP(actual: string) {
    let jsonstring;
    localStorage.removeItem('publicaciones');
    this.publicaciones.splice(this.publicaciones.findIndex(obj => obj.id.toString() === actual.toString()), 1);
    for (const elemento of this.publicaciones) {
      jsonstring = JSON.stringify(elemento);
      if (localStorage.getItem('publicaciones') !== null) {
        localStorage.setItem('publicaciones', localStorage.getItem('publicaciones') + ',' + jsonstring);
      }else {
        localStorage.setItem('publicaciones', jsonstring);
      }
    }
  }

  mostrarEliminar(actual: string){
    this.id = actual;
    if (this.eliminar === false) {
      this.eliminar = true;
    }else {
      this.eliminar = false;
    }
  }
}
