import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService, Usuario } from '../servicios/login.service';
import { PublicacionService, Publicacion } from '../servicios/publicacion.service';

@Component({
  selector: 'app-nueva-publicacion',
  templateUrl: './nueva-publicacion.component.html',
  styleUrls: ['./nueva-publicacion.component.css']
})
export class NuevaPublicacionComponent implements OnInit {
  usuario: Usuario;
  publicacion: Publicacion = {
    id: 0,
    titulo: '',
    cont: '',
    img: '',
    fecha: '',
    likes: 0,
    dislikes: 0,
  };

  constructor(private router: Router, private loginService: LoginService, private publicacionService: PublicacionService) { }

  ngOnInit() {
    this.usuario = JSON.parse(localStorage.getItem('usuario'));
    if (this.usuario === null || this.usuario.user !== 'admin') {
      this.router.navigate(['inicio']);
    }
  }

  publicar(titulo, cont, img: string) {
    this.publicacion.id = 1;
    if (localStorage.getItem('publicaciones') !== null) {
      this.publicacion.id = this.publicacionService.getId();
    }
    this.publicacion.titulo = titulo;
    this.publicacion.cont = cont;
    this.publicacion.img = img.replace(/^.*\\/, "");
    this.publicacion.fecha = new Date().getDate().toString() + '-' + (new Date().getMonth()+1).toString() + '-' + new Date().getFullYear().toString();
    const jsonstring = JSON.stringify(this.publicacion);
    if (localStorage.getItem('publicaciones') !== null) {
      localStorage.setItem('publicaciones', localStorage.getItem('publicaciones') + ',' + jsonstring);
    }else {
      localStorage.setItem('publicaciones', jsonstring);
    }
    this.router.navigate(['inicio']);
    console.log(this.publicacion);
  }

}
