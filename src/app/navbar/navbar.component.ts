import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LoginService, Usuario } from '../servicios/login.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  usuario: Usuario = {
    user: '',
    pass: '',
    rol: '',
  };

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private loginService: LoginService) { }

  ngOnInit() {
    if (this.usuario.user === '') {
      this.usuario = JSON.parse(localStorage.getItem('usuario'));
    }
    /*if (this.usuario === null) {
      this.usuario = {
        user: '',
        pass: '',
        rol: '',
      };
    }*/
  }

  cerrarSesion() {
    localStorage.removeItem('usuario');
    window.location.reload();
  }
}
