import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PublicacionService, Publicacion } from '../servicios/publicacion.service';

@Component({
  selector: 'app-publicacion',
  templateUrl: './publicacion.component.html',
  styleUrls: ['./publicacion.component.css']
})
export class PublicacionComponent implements OnInit {
  publiNumber: number;
  publi: any;

  constructor(private activatedRoute: ActivatedRoute, private publicacionService: PublicacionService) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(publi => {
      this.publiNumber = publi['publicacion'];
    });
    this.publi = this.publicacionService.devolverPubli(this.publiNumber);
  }

}
