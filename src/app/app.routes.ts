import { RouterModule, Routes } from '@angular/router';
import { InicioComponent } from './inicio/inicio.component';
import { SesionComponent } from './sesion/sesion.component';
import { PublicacionComponent } from './publicacion/publicacion.component';
import { AcercaDeComponent } from './acerca-de/acerca-de.component';
import { NuevaPublicacionComponent } from './nueva-publicacion/nueva-publicacion.component';

const APP_ROUTES: Routes = [
    { path: 'inicio', component: InicioComponent},
    { path: 'sesion', component: SesionComponent},
    { path: 'acerca', component: AcercaDeComponent},
    { path: 'nuevapublicacion', component: NuevaPublicacionComponent},
    { path: 'publicacion/:publicacion', component: PublicacionComponent},
    { path: '**', pathMatch: 'full', redirectTo: 'inicio'}
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
