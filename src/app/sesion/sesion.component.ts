import { Component, OnInit } from '@angular/core';
import { LoginService, Usuario } from '../servicios/login.service';
import { Router } from '@angular/router';
import { NavbarComponent } from '../navbar/navbar.component';

@Component({
  selector: 'app-sesion',
  templateUrl: './sesion.component.html',
  styleUrls: ['./sesion.component.css']
})
export class SesionComponent implements OnInit {
  entrar: boolean;

  constructor(private loginService: LoginService, private router: Router, private navbar: NavbarComponent) { }

  ngOnInit() {
    if ((JSON.parse(localStorage.getItem('usuario'))) !== null) {
      this.router.navigate(['inicio']);
    }
  }

  btnEntrar(user: string, pass: string) {
    this.entrar = this.loginService.compUsuario(user, pass);
    if (this.entrar === true) {
      this.router.navigate(['inicio']);
      window.location.reload();
    }else {
      this.entrar = false;
    }
  }

}

