import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { InicioComponent } from './inicio/inicio.component';
import { APP_ROUTING } from './app.routes';
import { SesionComponent } from './sesion/sesion.component';
import { LoginService } from './servicios/login.service';
import { PublicacionService } from './servicios/publicacion.service';
import { PublicacionComponent } from './publicacion/publicacion.component';
import { AcercaDeComponent } from './acerca-de/acerca-de.component';
import { NuevaPublicacionComponent } from './nueva-publicacion/nueva-publicacion.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    InicioComponent,
    SesionComponent,
    PublicacionComponent,
    AcercaDeComponent,
    NuevaPublicacionComponent
  ],
  imports: [
    BrowserModule,
    APP_ROUTING,
  ],
  providers: [
    LoginService,
    PublicacionService,
    NavbarComponent,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
